#include <LiquidCrystal.h>
#include "specialCharacters.h"
#include "Button.h"
#include <limits.h>
#include <string.h>

enum state {
    PCINFO_STATE,
    CLOCK_STATE,
    CPU_STATE,
    RAM_STATE,
    NETWORK_STATE,
    THERMAL_STATE,
    MEDIA_STATE,
};

enum request {
    NAME_REQUEST    = 0x11,
    CPU_REQUEST     = 0x21,
    RAM_REQUEST     = 0x22,
    NET_REQUEST     = 0x23,
    MEDIA_REQUEST   = 0x31,
    THERMAL_REQUEST = 0x41,
    CLOCK_REQUEST   = 0x51,
};

#define PIN_BUTTON_A 7
#define LEN_STATES   7
#define REQUEST_TIMEOUT 2000

typedef void (*refreshFunc) ();

void printPerc (int l, int perc);
void readSplit (int *format, int **var);
void pcinfo_refresh ();
void media_refresh ();
void clock_refresh ();
void cpu_refresh ();
void ram_refresh ();
void thermal_refresh (); 
void net_refresh ();

const refreshFunc refresh[LEN_STATES] = {
    pcinfo_refresh,
    clock_refresh,
    cpu_refresh,
    ram_refresh,
    net_refresh,
    thermal_refresh,
    media_refresh
};

const char splash[LEN_STATES][17] = {
    "Name/OS",
    "Clock/Date",
    "CPU",
    "Memory",
    "Net Speed",
    "CPU Temperature",
    "Media"
};

const int interval[LEN_STATES] = {
    INT_MAX,
    1000,
    3000,
    1000,
    1000,
    2000,
    3000,
};

LiquidCrystal lcd (12, 11, 5, 4, 3, 2);
Button buttonA (PIN_BUTTON_A);

state currentState;

void printPerc (int l, int perc) {
    int num = 16 * perc / 100;
    int i;
    lcd.setCursor (0, l);
    lcd.write (byte (1));
    for (i = 1; i < 15; i++) lcd.write (byte (2));
    lcd.write (byte (3));
    lcd.setCursor (0, l);
    for (i = 0; i < num; i++) lcd.write (byte (0));
}

void printCenter (int l, char *s) {
    int spc = (16 - strlen (s)) / 2;
    lcd.clear ();
    lcd.setCursor (spc, l);
    lcd.write (s);
}

void readSplit (int *format, int **var) {
    int i, j;
    int sum = 0;
    
    union {
        uint32_t bits;
        struct {
            uint8_t a;
            uint8_t b;
            uint8_t c;
            uint8_t d;
        } bytes;
    } data;

    for (i = 0; format[i] != 0; i++);
    i--;

    data.bytes.a = Serial.read ();
    data.bytes.b = Serial.read ();
    data.bytes.c = Serial.read ();
    data.bytes.d = Serial.read ();
    
    for (; i >= 0; i--) {
        int power = format[i];
        if (var[i] != NULL) *(var[i]) = data.bits % (1 << power);
        data.bits >>= power;
    }
}

int waitData (int len) {
    unsigned long start = millis ();
    while (Serial.available () < len) {
        if (millis () > start + REQUEST_TIMEOUT) return 1;
    }
    return 0;
}

void pcinfo_refresh () {
    char str[33];
    char *name, *os;
    String s;
    Serial.write (NAME_REQUEST);
    s = Serial.readString ();

    strcpy (str, s.c_str ());
    name = strtok (str, "\n");
    os = strtok (NULL, "\n");

    lcd.clear ();
    lcd.setCursor (0, 0);
    lcd.write (name);
    lcd.setCursor (0, 1);
    lcd.write (os);
}

void media_refresh () {
    char str[50];
    char *artist, *song;
    String s;
    Serial.write (MEDIA_REQUEST);
    s = Serial.readString ();
    strcpy (str, s.c_str ());

    artist = strtok (str, "\n");
    song = strtok (NULL, "\n");

    lcd.clear ();
    lcd.setCursor (0, 0);
    lcd.write (artist);
    lcd.setCursor (0, 1);
    lcd.write (song);
}

void clock_refresh () {
    int day, month, year, hour, minute;
    int format[] = {5, 4, 12, 5, 6, 0};
    int *vars[] = {&day, &month, &year, &hour, &minute, NULL};
    lcd.clear ();
    Serial.write (CLOCK_REQUEST);
    if (waitData (4)) return;

    readSplit (format, vars);

    lcd.setCursor (0, 0);
    lcd.print ("   ");
    lcd.print (day / 10);
    lcd.print (day % 10);
    lcd.print ('/');
    lcd.print (month / 10);
    lcd.print (month % 10);
    lcd.print ('/');
    lcd.print (year);

    lcd.setCursor (0, 1);
    lcd.print ("     ");
    lcd.print (hour / 10);
    lcd.print (hour % 10);
    lcd.print (':');
    lcd.print (minute / 10);
    lcd.print (minute % 10);
}

void cpu_refresh () {
    Serial.write (CPU_REQUEST);
    if (waitData (2)) return;
    lcd.clear ();
    uint16_t perc = Serial.read ();
    perc += Serial.read () << 8;
    char line[17];
    sprintf (line, "CPU: %2d.%d%%", perc / 10, perc % 10);

    lcd.setCursor (0, 0);
    lcd.print (line);
    printPerc (1, perc / 10);
}

void ram_refresh () {
    lcd.clear ();
    Serial.write (RAM_REQUEST);
    if (waitData (2)) return;
    uint16_t used = Serial.read ();
    uint16_t total = Serial.read ();

    char line[17];
    sprintf (line, "Mem: %2d.%d/%2d.%d",
             used / 10,
             used % 10,
             total / 10,
             total % 10);

    lcd.setCursor (0, 0);
    lcd.print (line);

    printPerc (1, used * 100 / total);
}

void thermal_refresh () {
    int temperature;
    int format[] = {21, 11, 0};
    int *vars[] = {NULL, &temperature, NULL};
    Serial.write (THERMAL_REQUEST);
    if (waitData (4)) return;
    readSplit (format, vars);

    char line[17];
    sprintf (line, "Temp: %2d.%d%cC", temperature / 10, temperature % 10, 4);
    lcd.clear ();
    lcd.setCursor (0, 0);
    lcd.print (line);
}

void net_refresh () {
    int velUp, velDown, unitUp, unitDown;
    int format[] = {10, 10, 2, 2, 0};
    int *vars[] = {&velUp, &velDown, &unitUp, &unitDown, NULL};
    Serial.write (NET_REQUEST);
    if (waitData(4)) return;
    lcd.clear ();

    readSplit (format, vars);
    
    char line[17];
    sprintf (line, "Up   %4d %s/s",
             velUp,
             unitUp == 0 ? "B" :
             unitUp == 1 ? "kB" :
             unitUp == 2 ? "MB" : "GB");

    lcd.setCursor (0, 0);
    lcd.print (line);

    sprintf (line, "Down %4d %s/s",
             velDown,
             unitDown == 0 ? "B" :
             unitDown == 1 ? "kB" :
             unitDown == 2 ? "MB" : "GB");

    lcd.setCursor (0, 1);
    lcd.print (line);

}

void setup () {
    Serial.begin (9600);
    lcd.createChar (0, charFilledBlock);
    lcd.createChar (1, charBeginEmptyBlock);
    lcd.createChar (2, charMidEmptyBlock);
    lcd.createChar (3, charEndEmptyBlock);
    lcd.createChar (4, charDegrees);
    pinMode (13, OUTPUT);
    digitalWrite (13, LOW);
    lcd.begin(16, 2);
    buttonA.begin ();
    delay (500);
    
    lcd.setCursor (0, 0);
    lcd.print ("Iniciando");
    lcd.setCursor (0, 0);
    delay (500);

    currentState = PCINFO_STATE;
}

void loop () {
    unsigned long start = millis ();
    refresh[currentState] ();

    pinMode (13, LOW);
    while (millis () - start < interval[currentState]) {
        if (buttonA.pressed ()) {
            currentState = (state) (currentState + 1) % LEN_STATES;
            printCenter (0, splash[currentState]);
            break;
        }
    }
    pinMode (13, HIGH);
}