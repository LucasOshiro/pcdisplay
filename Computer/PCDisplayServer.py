#!/usr/bin/env python3

import psutil
import serial
import serial.tools.list_ports
import sys
import dbus
import re
import subprocess
from datetime import datetime
from socket import gethostname
from time import sleep
from serial.serialutil import SerialException

arduino = None

#Request constants
NAME_REQUEST    = 0x11
CPU_REQUEST     = 0x21
RAM_REQUEST     = 0x22
NET_REQUEST     = 0x23
MEDIA_REQUEST   = 0x31
THERMAL_REQUEST = 0x41
CLOCK_REQUEST   = 0x51

def getOS():
    '''Returns the name of the operating system'''
    os = ''
    if sys.platform == 'linux':
        os_release = open('/etc/os-release', 'r')
        line = os_release.readline()
        os = line.replace('NAME="', '').replace('"', '')
        
    return os

def notify(text):
    subprocess.Popen(['notify-send', 'PCDisplay', text])

def sendName():
    '''Sends to arduino the name of the computer, and the name of the OS'''
    global arduino
    global computer_name
    global computer_os
    arduino.write((computer_name + '\n' + computer_os).encode())

def sendCPU():
    '''Sends to arduino the CPU usage'''
    global arduino
    perc = psutil.cpu_percent(0)
    arduino.write((int(psutil.cpu_percent (1) * 10)).to_bytes(2, byteorder='little'))

def sendRAM():
    '''Sends to arduino the RAM usage'''
    global arduino
    total = psutil.virtual_memory().total / 1024 ** 3
    used = psutil.virtual_memory().used / 1024 ** 3
    arduino.write(((int(total * 10) << 8) + int (used * 10)).to_bytes(2, byteorder='little'))

def sendThermal():
    global arduino
    temp = float(list(filter(lambda x: len(x) > 0,
           list(filter(lambda x: 'Physical id 0' in x,
           subprocess.check_output ('sensors').decode('utf-8').split('\n')))[0].split(' ')))[3].replace('°C',''))

    arduino.write(int(temp * 10).to_bytes(4, byteorder='little'))

def sendMedia():
    '''Sends to arduino the current playing song'''
    global arduino
    global bus
    playerName = None

    for x in bus.list_names():
        if re.match('org.mpris.MediaPlayer2.', x):
            playerName = x

    try:
        player = dbus.SessionBus().get_object(playerName, '/org/mpris/MediaPlayer2')
        interface = dbus.Interface(player, dbus_interface='org.mpris.MediaPlayer2.Player')
        metadata = player.Get('org.mpris.MediaPlayer2.Player',
                              'Metadata',
                              dbus_interface='org.freedesktop.DBus.Properties')

        if 'xesam:artist' in metadata and 'xesam:title' in metadata and len(metadata['xesam:artist']) > 0:
            artist = metadata['xesam:artist'][0]
            song = metadata['xesam:title']
            arduino.write((artist + '\n' + song[:20]).encode())
        else:
            arduino.write('No info\n'.encode())

    except dbus.exceptions.DBusException:
        arduino.write('Player not found\n '.encode())

def sendNet():
    '''Sends to arduino the upload and download speed'''
    global arduino
    dt = 0.1
    counters = psutil.net_io_counters()

    oldUp = counters.bytes_sent
    oldDown = counters.bytes_recv

    sleep (dt)

    counters = psutil.net_io_counters()

    newUp = counters.bytes_sent
    newDown = counters.bytes_recv

    velUp = (newUp - oldUp) / dt
    velDown = (newDown - oldDown) / dt

    unitUp = 0
    unitDown = 0

    while velUp >= 1024:
        velUp /= 1024
        unitUp += 1

    while velDown >= 1024:
        velDown /= 1024
        unitDown += 1

    arduino.write((int(velUp) << 14 | int(velDown) << 4 | unitUp << 2 | unitDown).to_bytes(4, byteorder='little'))

def sendClock():
    '''Sends to arduino the current clock and date'''
    global arduino
    now = datetime.now()
    arr = (now.minute | now.hour << 6 | now.year << 11 | now.month << 23 | now.day << 27).to_bytes(4, byteorder='little')
    arduino.write(arr)

# Mapping the event functions to the requests
event = {
    NAME_REQUEST:    sendName,
    CPU_REQUEST:     sendCPU,
    RAM_REQUEST:     sendRAM,
    NET_REQUEST:     sendNet,
    MEDIA_REQUEST:   sendMedia,
    THERMAL_REQUEST: sendThermal,
    CLOCK_REQUEST:   sendClock
}

# Gathering info about the computer
computer_name = gethostname()
computer_os = getOS()
bus = dbus.SessionBus()

def main():
    global arduino
    global computer_name
    global computer_os
    global dbus

    while True:
        # Prevent conflicts
        sleep(2)

        # List of all arduinos connected
        arduinoList = list(map(lambda x: x.device,
                               filter(lambda x: False if x.manufacturer == None else 'Arduino' in x.manufacturer,
                                      serial.tools.list_ports.comports())))

        if len(arduinoList) > 1:
            print('More than one arduino found. Using the first')
        elif len(arduinoList) < 1:
            print('Arduino not found')
            continue

        arduino = serial.Serial(arduinoList[0], 9600)
        print('Arduino connected on %s' % arduinoList[0])

        notify('Arduino connected on ' + arduinoList[0])

        while True:
            try:
                req = arduino.read()[0]
                if req in event: event[req]()
            except SerialException:
                notify('Connection lost')
                break

if __name__ == '__main__':
    main()
